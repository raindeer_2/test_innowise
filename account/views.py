from rest_framework.permissions import IsAuthenticated
from account.models import UserProfile
from account.permissions import IsOwnerProfileOrReadOnly
from account.serializers import UserProfileSerializer
from rest_framework import viewsets


class UserProfileListCreateView(viewsets.ModelViewSet):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        user = self.request.user
        serializer.save(user=user)


class UserProfileDetailView(viewsets.ModelViewSet):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer
    permission_classes = [IsOwnerProfileOrReadOnly, IsAuthenticated]
