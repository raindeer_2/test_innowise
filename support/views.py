from support.models import Tickets, Message
from rest_framework import viewsets
from support.serializers import TicketSerializer, MessageSerializer
from rest_framework.response import Response
from rest_framework import status

from support.service_send_mail import send_message_ticket_created
from support.tasks import ticket_created
from rest_framework.permissions import IsAuthenticated


class TicketViewSet(viewsets.ModelViewSet):
    queryset = Tickets.objects.all()
    serializer_class = TicketSerializer
    permission_classes = [IsAuthenticated]

    def create(self, request):
        serialized_data = self.serializer_class(data=request.data)
        serialized_data.is_valid(raise_exception=True)
        ticket = serialized_data.save()
        send_message_ticket_created(ticket.id)
        return Response({
            'status': status.HTTP_201_CREATED
        })


class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
