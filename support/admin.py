from django.contrib import admin

from support.models import Tickets


@admin.register(Tickets)
class TicketAdmin(admin.ModelAdmin):
    list_display = ('id', 'first_name', 'last_name', 'email', 'problem', 'status')
    list_filter = ['status']
