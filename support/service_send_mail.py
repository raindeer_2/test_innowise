from support.tasks import ticket_created


def send_message_ticket_created(ticket_id):
    ticket_created.delay(ticket_id)
