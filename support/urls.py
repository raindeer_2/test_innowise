from . import views
from rest_framework import routers

app_name = 'ticketss'

router = routers.SimpleRouter()

router.register('ticket', views.TicketViewSet, basename='create')
router.register('message',views.MessageViewSet, basename='message')

urlpatterns = router.urls
