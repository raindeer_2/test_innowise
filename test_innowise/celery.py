import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'test_innowise.settings')

app = Celery('test_innowise')

app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks()
